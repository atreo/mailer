<?php

namespace Atreo\Mailer\DI;

use Nette\DI\CompilerExtension;
use Nette\Utils\Validators;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class MailerExtension extends CompilerExtension
{

	/**
	 * @var array
	 */
	private $defaults = [
		'templatesDir' => '%appDir%/Templates/@emails',
		'catchExceptions' => TRUE,
		'sender' => FALSE
	];



	public function loadConfiguration()
	{
		$config = $this->getConfig($this->defaults);

		Validators::assertField($config, 'templatesDir', 'string');
		Validators::assertField($config, 'catchExceptions', 'bool');
		Validators::assertField($config, 'sender', 'string|bool');

		$builder = $this->getContainerBuilder();

		$builder->addDefinition($this->prefix('mailer'))
			->setClass('Atreo\Mailer\Mailer', [
				$config['templatesDir']
			]);

		$sender = $builder->addDefinition($this->prefix('sender'));
		$sender->setClass('Atreo\Mailer\MailerSender', [
				$config['catchExceptions']
			]);

		if ($config['sender']) {
			$sender->setInject(FALSE)
			->setAutowired(FALSE);
		}
	}

}
