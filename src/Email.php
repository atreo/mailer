<?php

namespace Atreo\Mailer;

use Nette\Mail\Message;
use Nette\Utils\Strings;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class Email extends \Nette\Object
{

	/**
	 * @var string
	 */
	private $subject;

	/**
	 * @var string
	 */
	private $fromEmail;

	/**
	 * @var string
	 */
	private $fromName;

	/**
	 * @var string
	 */
	private $body;

	/**
	 * @var array
	 */
	private $recipients = [];

	/**
	 * @var array
	 */
	private $copies = [];

	/**
	 * @var array
	 */
	private $blindCopies = [];

	/**
	 * @var string
	 */
	private $utmSource = NULL;

	/**
	 * @var string
	 */
	private $utmMedium = NULL;

	/**
	 * @var string
	 */
	private $utmCampaign = NULL;

	/**
	 * @var string
	 */
	private $utmContent = NULL;

	/**
	 * @var string
	 */
	private $templateName;

	/**
	 * @var array
	 */
	private $templateParams = [];



	/**
	 * @return array
	 */
	public function getRecipients()
	{
		return $this->recipients;
	}



	/**
	 * @param string $email
	 * @param string|NULL $name
	 * @return $this
	 */
	public function addRecipient($email, $name = NULL)
	{
		$this->recipients[$email] = $name;
		return $this;
	}



	/**
	 * @param string $email
	 * @param string|NULL $name
	 * @return $this
	 */
	public function addCopy($email, $name = NULL)
	{
		$this->copies[$email] = $name;
		return $this;
	}



	/**
	 * @param string $email
	 * @param string|NULL $name
	 * @return $this
	 */
	public function addBlindCopy($email, $name = NULL)
	{
		$this->blindCopies[$email] = $name;
		return $this;
	}



	/**
	 * @return string
	 */
	public function getUtmSource()
	{
		return $this->utmSource;
	}



	/**
	 * @param string $utmSource
	 */
	public function setUtmSource($utmSource)
	{
		$this->utmSource = $utmSource;
	}



	/**
	 * @return string
	 */
	public function getUtmMedium()
	{
		return $this->utmMedium;
	}



	/**
	 * @param string $utmMedium
	 */
	public function setUtmMedium($utmMedium)
	{
		$this->utmMedium = $utmMedium;
	}



	/**
	 * @return string
	 */
	public function getUtmCampaign()
	{
		return $this->utmCampaign;
	}



	/**
	 * @param string $utmCampaign
	 */
	public function setUtmCampaign($utmCampaign)
	{
		$this->utmCampaign = $utmCampaign;
	}



	/**
	 * @return string
	 */
	public function getUtmContent()
	{
		return $this->utmContent;
	}



	/**
	 * @param string $utmContent
	 */
	public function setUtmContent($utmContent)
	{
		$this->utmContent = $utmContent;
	}



	/**
	 * @return string
	 */
	public function getSubject()
	{
		return $this->subject;
	}



	/**
	 * @param string $subject
	 * @return $this
	 */
	public function setSubject($subject)
	{
		$this->subject = $subject;
		return $this;
	}



	/**
	 * @return string
	 */
	public function getBody()
	{
		return $this->body;
	}



	/**
	 * @param string $body
	 * @return $this
	 */
	public function setBody($body)
	{
		$this->body = $body;
		return $this;
	}



	/**
	 * @return string
	 */
	public function getTemplateName()
	{
		return $this->templateName;
	}



	/**
	 * @return array
	 */
	public function getTemplateParams()
	{
		return $this->templateParams;
	}



	/**
	 * @param array $params
	 * @param string|NULL $templateName
	 * @return $this
	 */
	public function setTemplateBody(array $params = [], $templateName = NULL)
	{
		$this->templateParams = $params;

		if ($templateName) {
			$this->templateName = $templateName;
		} else {
			$name = get_class($this);

			if (Strings::match($name, "#\\\\#")) {
				$parts = Strings::split(get_class($this), '#\\\\#');
				$name = end($parts);
			}

			$this->templateName = Strings::firstLower($name);
		}

		return $this;
	}



	/**
	 * @param string $email
	 * @param string|NULL $name
	 */
	public function setFrom($email, $name = NULL)
	{
		$this->fromEmail = $email;
		$this->fromName = $name;
	}



	/**
	 * @return \Nette\Mail\Message
	 */
	public function toMessage()
	{
		$message = new Message();
		$message->subject = $this->subject;

		if ($this->fromEmail) {
			$message->setFrom($this->fromEmail, $this->fromName);
		}

		foreach ($this->recipients as $email => $name) {
			$message->addTo($email, $name);
		}

		foreach ($this->copies as $email => $name) {
			$message->addCc($email, $name);
		}

		foreach ($this->blindCopies as $email => $name) {
			$message->addBcc($email, $name);
		}

		$message->setHtmlBody($this->body);
		return $message;
	}

}
