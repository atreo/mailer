<?php

namespace Atreo\Mailer;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
interface ISender
{

	/**
	 * @param \Atreo\Mailer\Email $email
	 */
	public function send(Email $email);



	/**
	 * @return boolean
	 */
	public function getCatchExceptions();



	/**
	 * @param boolean $catch
	 * @return void
	 */
	public function setCatchExceptions($catch);

}
