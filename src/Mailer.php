<?php

namespace Atreo\Mailer;

use Atreo\UI\TemplateGenerator;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class Mailer extends \Nette\Object
{

	/**
	 * @var string
	 */
	private $templatesDir;

	/**
	 * @var \Atreo\Translator\Translator
	 */
	private $translator;

	/**
	 * @var ISender
	 */
	private $sender;

	/**
	 * @var \Atreo\UI\TemplateGenerator
	 */
	private $templateGenerator;



	/**
	 * @param string $templatesDir
	 * @param ISender $sender
	 * @param \Atreo\UI\TemplateGenerator $templateGenerator
	 */
	public function __construct($templatesDir, ISender $sender, TemplateGenerator $templateGenerator)
	{
		$this->sender = $sender;
		$this->templateGenerator = $templateGenerator;
		$this->templatesDir = $templatesDir;
	}



	public function send(Email $email)
	{
		if ($templateName = $email->getTemplateName()) {
			$template = $this->templateGenerator->generate();

			$templateFile = $this->templatesDir . '/' . ($this->translator ? $this->translator->getLocale() . '/' : '') . $email->getTemplateName() . '.latte';
			$template->setFile($templateFile);

			$template->utms = [
				'utm_source' => $email->getUtmSource(),
				'utm_medium' => $email->getUtmMedium() ? $email->getUtmMedium() : 'email',
				'utm_campaign' => $email->getUtmCampaign() ? $email->getUtmCampaign() : $email->getTemplateName(),
				'utm_content' => $email->getUtmContent(),
			];

			foreach ($email->getTemplateParams() as $key => $value) {
				$template->$key = $value;
			}

			$email->setBody((string) $template);
		}

		if ($this->translator && method_exists($this->sender, 'setTranslator')) {
			$this->sender->setTranslator($this->translator);
		}

		$this->sender->send($email);
	}



	/**
	 * @return \Atreo\Translator\Translator
	 */
	public function getTranslator()
	{
		return $this->translator;
	}



	/**
	 * @param \Atreo\Translator\Translator $translator
	 */
	public function setTranslator($translator)
	{
		$this->translator = $translator;
	}

}
