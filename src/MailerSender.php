<?php

namespace Atreo\Mailer;

use Nette\Mail\IMailer;
use Tracy\Debugger;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class MailerSender implements ISender
{
	/**
	 * @var bool
	 */
	private $catchExceptions;

	/**
	 * @var \Nette\Mail\IMailer
	 */
	private $mailer;

	/**
	 * @var \Atreo\Translator\Translator
	 */
	private $translator;



	/**
	 * @param bool $catchExceptions
	 * @param IMailer $mailer
	 */
	public function __construct($catchExceptions, IMailer $mailer)
	{
		$this->mailer = $mailer;
		$this->catchExceptions = $catchExceptions;
	}



	/**
	 * @return \Atreo\Translator\Translator
	 */
	public function getTranslator()
	{
		return $this->translator;
	}



	/**
	 * @param \Atreo\Translator\Translator $translator
	 */
	public function setTranslator($translator)
	{
		$this->translator = $translator;
	}



	/**
	 * @return \Nette\Mail\IMailer
	 */
	public function getMailer()
	{
		return $this->mailer;
	}



	/**
	 * @param \Nette\Mail\IMailer $mailer
	 */
	public function setMailer($mailer)
	{
		$this->mailer = $mailer;
	}



	/**
	 * @return bool
	 */
	public function getCatchExceptions()
	{
		return $this->catchExceptions;
	}



	/**
	 * @param boolean $catch
	 */
	public function setCatchExceptions($catch)
	{
		$this->catchExceptions = $catch;
	}



	/**
	 * @param \Atreo\Mailer\Email $email
	 * @throws \Exception
	 */
	public function send(Email $email)
	{

		try {
			$message = $email->toMessage();
			if ($this->translator) {
				$message->setSubject($this->translator->translate($message->getSubject()));
				$message->setBody($this->translator->translate($message->getBody()));
				$message->setHtmlBody($this->translator->translate($message->getHtmlBody()));
			}
			$this->mailer->send($message);
		} catch (\Exception $e) {
			if ($this->catchExceptions) {
				Debugger::log($e, Debugger::CRITICAL);
			} else {
				throw $e;
			}
		}

	}
	
}
