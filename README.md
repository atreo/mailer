Atreo/Mailer
===========================


Installation
------------

```sh
$ composer require atreo/mailer
```

Extension configuration:

```
mailer: Atreo\Mailer\DI\MailerExtension

mailer:
	templatesDir: %appDir%/Templates/@emails
	catchExceptions: true
	sender: false # or your custom sender for example: @rabbitMqSender

```

Use mailer:

```php

/**
 * @inject
 * @var \Atreo\Mailer\Mailer
 */
public $mailer;

// you can use basic email class

$email = new Email();
$email->addRecipient('test@test.cz', 'Testovací Uživatel');
$email->setBody('Testovací email');
$this->mailer->send($email);

// or you can use your own email class

$this->mailer->send(new ForgottenPasswordEmail($user, $resetKey));

// class definition

class ForgottenPasswordEmail extends Email
{

	/**
	 * @param \App\Entity\User $user
	 */
	public function __construct(User $user)
	{
		$this->setSubject("Obnovení hesla");
		$this->addRecipient($user->email, $user->name);
		$this->setTemplateBody(['user' => $user]);
	}

}


```